#!/bin/sh

set -e

envsubst < /etcnginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'